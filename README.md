# sliderEmote
Replace default slider emoticone by own.

## Copyright
- Copyright © 2015-2017 Denis Chenu <http://sondages.pro>
- Licence : GNU Afferor General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
