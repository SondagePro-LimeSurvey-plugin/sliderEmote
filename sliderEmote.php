<?php
  /**
  *
  * @author Denis Chenu <denis@sondages.pro>
  * @copyright 2015-2017 Denis Chenu <http://sondages.pro>
  * @copyright 2015 OB&C Management Consulting <http://obc-managementconsulting.fr/>
  * @license AGPL v3
  * @version 1.1.4
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU Affero General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  */
  class sliderEmote extends \ls\pluginmanager\PluginBase
  {
    protected $storage = 'DbStorage';
    static protected $description = 'Replace default slider emoticone by own.';
    static protected $name = 'sliderEmote';

    private $aBaseColor=array(
      '#a94442',
      '#8a6d3b',
      '#31708f',
      '#3c763d',
    );
    public function init()
    {
      $this->subscribe('beforeQuestionRender');
      $this->subscribe('beforeSurveySettings');
      $this->subscribe('newSurveySettings');
    }

    public function beforeQuestionRender()
    {
      $oEvent = $this->getEvent();
      $iSurveyId=$oEvent->get('surveyId');

      if($oEvent->get('type')=="5" && $this->get('active', 'Survey', $oEvent->get('surveyId'),true))
      {
        $iQid=$oEvent->get('qid');
        $oAttributeSlider=QuestionAttribute::model()->find("qid=:qid and attribute=:attribute",array(':qid'=>$iQid,':attribute'=>'slider_rating'));
        if($oAttributeSlider && $oAttributeSlider->value=="2")
        {
          $cssUrl=Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets');
          Yii::app()->getClientScript()->registerCssFile($cssUrl . '/ratingEmote.css');
          Yii::app()->getClientScript()->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/ratingEmote.js'));
          $sType=$this->get('system', 'Survey', $iSurveyId,'col');
          if($sType=="image")
          {
            $aSettings=array(
              'type'=>'images',
              'imagesUrls'=>$this->getImageUrl($iSurveyId),
            );
            App()->getClientScript()->registerScript('ratingEmoteSettings','var ratingEmote='.json_encode($aSettings).";",CClientScript::POS_HEAD);
          }
          elseif($cssColor=$this->registerColorStyle($iSurveyId))
          {
            Yii::app()->getClientScript()->registerCss('cssColorEmote',$cssColor);
          }
        }
      }
      //if($oEvent->get('type')
    }

    public function beforeSurveySettings()
    {
        $event = $this->event;
        // Get the content of actual icon
        // DO this in CSS only ?
        $cssUrl=Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets');
        Yii::app()->getClientScript()->registerCssFile($cssUrl . '/ratingEmote.css');
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/adminSettings.js'));

        // find actual image url
        $aImageUrl=$this->getImageUrl($event->get('survey'));
        $demoHtml="<p>Couleur</p><ul class='emotes-list emotes-color answers-list noread'><li class='emote-rating emote' data-value='1'><a title='1' class='emote-1'>1</a></li><li class='emote-rating emote' data-value='2'><a title='2' class='emote-2'>2</a></li><li class='emote-rating emote' data-value='3'><a title='3' class='emote-3'>3</a></li><li class='emote-rating emote' data-value='4'><a title='4' class='emote-4'>4</a></li></ul>";
        $demoHtml.="<p>Images</p><ul class='emotes-list emotes-images answers-list noread'>";
        for ($i = 1; $i <= 4; $i++) {
          $demoHtml.="<li class='emote-rating emote emote-{$i}'><img title='{$i}' src='{$aImageUrl[$i]}' alt='{$i}' /></li>";
        }
        $demoHtml.="</ul>";
        $event->set("surveysettings.{$this->id}", array(
          'name' => get_class($this),
          'settings' => array(
            'active'=>array(
              'type'=>'boolean',
              'label'=>'Activate',
              'current' => $this->get('active', 'Survey', $event->get('survey'),true),
            ),
            'system'=>array(
              'type'=>'select',
              'label'=>'Utilisation des ',
              'options'=>array(
                'col'=>'Couleurs',
                'image'=>'Images',
              ),
              'current' => $this->get('system', 'Survey', $event->get('survey'),'col'),
            ),
            'border-color'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"#929292",
                'data-type'=>'color',
              ),
              'label'=>'Couleur des bordures',
              'current' => $this->get('border-color', 'Survey', $event->get('survey'),''),
            ),
            'background-color'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"#ffffff",
                'data-type'=>'color',
              ),
              'label'=>'Couleur de fond',
              'current' => $this->get('background-color', 'Survey', $event->get('survey'),''),
            ),
            'emote-color-1'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"#a94442",
                'data-type'=>'color',
              ),
              'label'=>'Couleur (bordure au survol et fond sélectionné) emote 1',
              'current' => $this->get('emote-color-1', 'Survey', $event->get('survey'),''),
            ),
            'emote-color-2'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"#8a6d3b",
                'data-type'=>'color',
              ),
              'label'=>'Couleur (bordure au survol et fond sélectionné) emote 2',
              'current' => $this->get('emote-color-2', 'Survey', $event->get('survey'),''),
            ),
            'emote-color-3'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"#31708f",
                'data-type'=>'color',
              ),
              'label'=>'Couleurs (bordure au survol et fond sélectionné) emote 3',
              'current' => $this->get('emote-color-3', 'Survey', $event->get('survey'),''),
            ),
            'emote-color-4'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"#3c763d",
                'data-type'=>'color',
              ),
              'label'=>'Couleur (bordure au survol et fond sélectionné) emote 4',
              'current' => $this->get('emote-color-4', 'Survey', $event->get('survey'),''),
            ),
            'emote-background-transparency'=>array(
              'type'=>'float',
              'htmlOptions'=>array(
                'placeholder'=>"0.3",
                'step'=>"0.01",
                'min'=>'0',
                'max'=>'1',
              ),
              'label'=>'Transparence de la couleur de fond',
              'current' => $this->get('emote-background-transparency', 'Survey', $event->get('survey'),''),
              'help' => "La couleur de fond est celle de la bordure au moment ou est coché, permet d’ajouter une transparence"
            ),
            'emote-image-1'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"emote-1.png",
              ),
              'label'=>'Lien de l’image emote 1 <img src="'.$aImageUrl[1].'" alt="" />',
              'current' => $this->get('emote-image-1', 'Survey', $event->get('survey'),''),
            ),
            'emote-image-2'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"emote-2.png",
              ),
              'label'=>'Lien de l’image emote 2 <img src="'.$aImageUrl[2].'" alt="" />',
              'current' => $this->get('emote-image-2', 'Survey', $event->get('survey'),''),
            ),
            'emote-image-3'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"emote-3.png",
              ),
              'label'=>'Lien de l’image emote 3 <img src="'.$aImageUrl[3].'" alt="" />',
              'current' => $this->get('emote-image-3', 'Survey', $event->get('survey'),''),
            ),
            'emote-image-4'=>array(
              'type'=>'string',
              'htmlOptions'=>array(
                'placeholder'=>"emote-4.png",
              ),
              'label'=>'Lien de l’image emote 4 <img src="'.$aImageUrl[4].'" alt="" />',
              'current' => $this->get('emote-image-4', 'Survey', $event->get('survey'),''),
            ),
            'showActual'=>array(
              'type'=>'info',
              'content'=>"<p class='alert alert-info'><strong>Versions actuelles (mise à jour seulement après la sauvegarde)</strong></p>".$demoHtml."<p class='alert'>Attention : certaines différences peuvent intervenir selon le modèle</p>",
            ),
            'documentation'=>array(
              'type'=>'info',
              'content'=>"<p class='alert alert-info'>Pour les images : prend dans un premier temps : image de nom emote-1.png,emote-2.png,emote-3.png,emote-4.png dans les ressources du questionnaire, puis le texte indiqué dans le champs, puis le lien complet</p>"
            ),
          ),
        ));
        if($cssColor=$this->registerColorStyle($event->get('survey')))
        {
          Yii::app()->getClientScript()->registerCss('cssColorEmote',$cssColor);
        }
    }

    public function newSurveySettings()
    {
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value)
        {
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }
    private function hex2rgb($hex) {
      $hex = str_replace("#", "", $hex);

      if(strlen($hex) == 3) {
        $r = hexdec(substr($hex,0,1).substr($hex,0,1));
        $g = hexdec(substr($hex,1,1).substr($hex,1,1));
        $b = hexdec(substr($hex,2,1).substr($hex,2,1));
      } else {
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));
      }
      $rgb = array($r, $g, $b);
      //return implode(",", $rgb); // returns the rgb values separated by commas
      return $rgb; // returns an array with the rgb values
    }
    private function getImageUrl($iSurveyId)
    {
      $aImageUrl=array();
      for ($i = 1; $i <= 4; $i++) {
        $setting=$this->get("emote-image-{$i}", 'Survey', $iSurveyId,"emote-{$i}.png");
        if(substr( $setting, 0, 1 ) === "/" || substr( $setting, 0, 4 ) === "http")
          $aImageUrl[$i]=$setting;
        else
          $aImageUrl[$i]=App()->getConfig('uploadurl')."/surveys/{$iSurveyId}/images/".$setting;
      }
      return $aImageUrl;
    }
    private function registerColorStyle($iSurveyId)
    {
      $aSettings=array();
      if($this->get("border-color", 'Survey', $iSurveyId))
        $aSettings['.emotes-color li.emote-rating a']['border-color']=$this->get("border-color", 'Survey', $iSurveyId);
      if($this->get("background-color", 'Survey', $iSurveyId))
        $aSettings['.emotes-color li.emote-rating a']['background-color']=$this->get("background-color", 'Survey', $iSurveyId);
      $transparency=intval($this->get("emote-background-transparency", 'Survey',$iSurveyId));
      for ($i = 1; $i <= 4; $i++) {
        if($color=$this->get("emote-color-{$i}", 'Survey', $iSurveyId))
        {
          $aSettings[".emotes-color .emote-{$i}:hover"]['border-color']=$color;
          $aSettings[".emotes-color .emote-checked .emote-{$i}"]['background-color']=array(
            $color,
            "rgba(".implode(',',$this->hex2rgb($color)).','.(($transparency>0) ? $transparency : 0.3).")",
          );
        }
        elseif($transparency)
        {
          $aSettings[".emotes-color .emote-checked .emote-{$i}"]['background-color']="rgba(".implode(',',$this->hex2rgb($aBaseColor[$i-1])).','.$transparency.")";
        }
      }
      $css="";
      foreach($aSettings as $element=>$aSetting)
      {
        $css.="{$element}{";
        foreach($aSetting as $setting=>$values)
        {
          if(is_array($values))
          {
            foreach($values as $value)
              $css.="{$setting}:{$value};";
          }
          else
          {
            $css.="{$setting}:{$values};";
          }
        }
        $css.="}\n";
      }
      return $css;
    }
  }
?>
