/**
 * @file ratingEmote javascript part
 * @author Denis Chenu
 * @copyright Denis Chenu <http://www.sondages.pro>
 * @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL v3.0
 */

function doRatingSlider(qID)
{
  var ratingEmote = window.ratingEmote || {type:'color'};
  doRatingEmote(qID,ratingEmote);
}
function doRatingEmote(qID,ratingEmoteOptions)
{
  var answersList=$('#question'+qID+' .answers-list.radio-list:not(.slidered-list)');
  var type=ratingEmoteOptions.type;

  if(!answersList){ return;}
  if ((!$.support.opacity && !$.support.style)) try { document.execCommand("BackgroundImageCache", false, true)} catch(e) { };
  var htmlRatingEmote="<ul class='emotes-list emotes-"+type+" answers-list' aria-hidden='true'> ";
  for (i=1; i<5; i++) {
    if (typeof ratingEmoteOptions.imagesUrls != 'undefined')
      htmlRatingEmote+="<li class='emote-rating emote' data-value='"+i+"'><img title='"+i+"' src='"+ratingEmoteOptions.imagesUrls[i]+"' alt='"+i+"' class='emote-"+i+"' /></li>"
    else
      htmlRatingEmote+="<li class='emote-rating emote' data-value='"+i+"'><a title='"+i+"' class='emote-"+i+"'>"+i+"</a></li>"
  }
  htmlRatingEmote+="</li>";
  answersList.after(htmlRatingEmote);

  var emotesElement=$('#question'+qID+' .emotes-list');
  answersList.addClass("emoted-list hide sr-only");
  answersList.find("[value=5]").closest("li").remove();
  var openValue=answersList.find("input:radio:checked").val();
  if(openValue){
    var thisnum=openValue;
    emotesElement.children('.emote-rating[data-value='+thisnum+']').removeClass("emote-unchecked").addClass("emote-checked").siblings(".emote-rating" ).addClass("emote-unchecked");
  }
  $(document).on("click",'#question'+qID+' .emote-rating',function(){
    var thischoice=thisnum=$(this).attr('data-value');
    answersList.find("input.radio[value='"+thischoice+"']").click();
    emotesElement.children('.emote-rating').removeClass("emote-checked");
    emotesElement.children('.emote-rating[data-value='+thisnum+']').removeClass("emote-unchecked").addClass("emote-checked").siblings(".emote-rating" ).addClass("emote-unchecked");
  });
}
